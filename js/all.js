	
	//index
	var myVideo = document.getElementsByTagName('video')[0];
	var play=document.getElementById("video-play");
	var close=document.getElementById("video-modal-close");
	var videoModal=document.getElementById("video-modal");
	var videoContent=document.getElementById("videoContent");
	videoModal.style.display="none";
		play.onclick=function videoPlay(){
			videoModal.style.display="block";
			myVideo.play();
		}
		close.onclick=function videoClose(){
			videoModal.style.display="none";
			myVideo.pause();
		}

		//active


		


		//head
		var index=document.getElementById("index");
		var index_head=document.getElementById("index-head");
		var active=document.getElementById("active");
		var active_head=document.getElementById("active-head");
		var comp=document.getElementById("comp");
		var cp_head=document.getElementById("cp-head");
		active.style.display="none";
		comp.style.display="none";
		index_head.style.backgroundColor="#999";
		index_head.style.color="#000";
		active_head.onclick=function activeOn(){
			active.style.display="block";
			index.style.display="none";
			comp.style.display="none";
			active_head.style.backgroundColor="#999";
			active_head.style.color="#000";
			index_head.style.color="#ADADAD"
			index_head.style.backgroundColor="#fff";
			cp_head.style.backgroundColor="#fff";
			cp_head.style.color="#ADADAD";

		}
		index_head.onclick=function(){
			index.style.display="block";
			active.style.display="none";
			comp.style.display="none";
			index_head.style.backgroundColor="#999";
			index_head.style.color="#000";
			cp_head.style.backgroundColor="#fff";
			cp_head.style.color="#ADADAD";
			active_head.style.backgroundColor="#fff";
			active_head.style.color="#ADADAD";
		}
		cp_head.onclick=function(){
			index.style.display="none";
			active.style.display="none";
			comp.style.display="block";
			cp_head.style.backgroundColor="#999";
			cp_head.style.color="#000";
			active_head.style.backgroundColor="#fff";
			active_head.style.color="#ADADAD";
			index_head.style.color="#ADADAD"
			index_head.style.backgroundColor="#fff";
		}


		//登录框

		var signIn_Up=document.getElementById("signIn_Up");
		var sign=document.getElementById("sign");
		var signIn=document.getElementById("signIn");
		var signUp=document.getElementById("signUp");
		var formwrapper=document.getElementsByClassName("formwrapper");
		var signClose=document.getElementById("signClose");
		signIn_Up.style.display="none";
		sign.onclick=function(){
			signIn_Up.style.display="block";
			formwrapper[1].style.display="none";
			formwrapper[0].style.display="block";
			signIn.style.color="#0F88EB";
			signUp.style.color=" #D5D5D5";
		}
		signIn.onclick=function(){
			formwrapper[1].style.display="none";
			formwrapper[0].style.display="block";
			signIn.style.color="#0F88EB";
			signUp.style.color=" #D5D5D5";
		}
		signUp.onclick=function(){
			formwrapper[0].style.display="none";
			formwrapper[1].style.display="block";
			signIn.style.color="#D5D5D5";
			signUp.style.color="#0F88E8";
		}
		signClose.onclick=function(){
			signIn_Up.style.display="none";
		}




/*Ajax*/

		var headersId,headersToken,headersMail;
		var headersMatchId=1000;
		var host="/api/";
		var joinCp=false;//判断是否报名
		var sure=$("#sure");
		var alertText=$("#alertText");
		var signInOrUp=false;//帮段是否登录
		$("#bombBox").hide();
		$("#userOnline").hide();
		$("#cpJoin").hide();//报名框隐藏
		$("#upLoadForm").hide();//上传文件隐藏

		/*加载页面cookie*/

		$(document).ready(function(){
			$.ajax({
				url:host+"/match",
				type:"get",
				dataType:"json",
				contentType:"application/json",
				success:function(data){
					if(data.status==="LOGIN_SUCCESS"){
						headersMatchId=data.id;
						return headersMatchId;
					}else if(data.status==="USER_NOT_EXIST"){
						alert("该用户未注册");
						return false;
					}
				}
			});
			$.ajax({
				url:host+"user/online",
				type:"get",
				success:function(data){
					if(data===true){
						headersMail=$.cookie("cookieEmailOne");
						headersToken=$.cookie("cookieToken");
						headersId=$.cookie("cookieUserId");
   		 				$("#userOnline").show();
	   		 	  		$("#userId").text($.cookie("cookieEmailOne"));
	   		 	  		$("#signIn_Up").hide();
	   		 	  		$("#sign").hide();
	   		 	  		$("#cpJoin").show();
	   		 	  		$("#joinInCp").hide();
	   		 	  		$("#upLoadForm").show();
						$.ajax({
							url:host+"match/dataSet",
							type:"get",
							dataType:"json",
							contentType:"application/json",
							headers:{
								"open-day-user":headersMail,
								"open-day-token":headersToken,
								"open-day-user-id":headersId,
								"open-day-match-id":headersMatchId
							},
							success:function(data){
								$("#downLink").text("数据集下载地址：");
								$("#hrefCloud").text(data.dataLink);
								$("#hrefCloud").attr("href",data.dataLink);
								$("#downPassword").text("数据集下载密码："+data.dataPassword);
							}
						});
						/*获取队伍信息*/
						$.ajax({
							url:host+"match/team",
							type:"get",
							dataType:"json",
							contentType:"application/json;charset=UTF-8",
							headers:{
								"open-day-user":headersMail,
								"open-day-token":headersToken,
								"open-day-user-id":headersId,
								"open-day-match-id":headersMatchId
							},
							success:function(data){
								$("#msgTeam").text("队伍名："+data.teamName);
								$("#msgCaptain").text("队长名："+data.captain);
								$("#msgCaptainEmail").text("队长邮箱："+data.captainMail);
								$("#msgMemberEmail").text("队员邮箱："+data.teamMemberMail);
								$("#cpJoin").hide();
							}
						});								
	   		 	  		
					}
				}
			});
		});

		//用户登录	
		$("#bottonSignIn").click(function(){
			var userEmailOne=$("#userEmailOne").val();
			var userPasswordOne=$("#userPasswordOne").val();
			if(userEmailOne===""){
				alert("邮箱不能为空");
				return false;
			}
			if(userPasswordOne===""){
				alert("密码不能为空");
				return false;
			}else{
				if (userEmailOne!=""&&userPasswordOne!=""){
					/*获取比赛id值*/
					$.ajax({
						url:host+"/match",
						type:"get",
						dataType:"json",
						contentType:"application/json",
						success:function(data){
							if(data.status==="LOGIN_SUCCESS"){
								headersMatchId=data.id;
								return headersMatchId;
							}else if(data.status==="USER_NOT_EXIST"){
								alert("该用户未注册");
								return false;
							}
						}
					});
					/*用户登录成功*/
					$.ajax({
        				url:'/api/user/signin',
        				type:"post",
        				contentType:"application/json",
        				data:JSON.stringify({
    						"mail":userEmailOne,
    						"password":$.md5(userPasswordOne)
    					}),
    					success: function(data) {
    	   		 			if(data.mail===userEmailOne){
    	   		 				signInOrUp=true;
    	   		 				$("#captainEmail").text(userEmailOne);
    	   		 				$("#userOnline").show();
        	   		 	  		$("#userId").text(userEmailOne);
        	   		 	  		$("#signIn_Up").hide();
        	   		 	  		$("#sign").hide();
        	   		 	  		$("#cpJoin").show();
        	   		 	  		$("#joinInCp").hide();
        	   		 	  		$("#upLoadForm").show();
        	   		 	  		headersId=data.id;
        	   		 	  		headersMail=data.mail;
        	   		 	  		headersToken=data.token;
        	   		 	  		$.cookie("cookieEmailOne", userEmailOne, { expires: 7 }); 
        	   		 	  		$.cookie("cookieToken",headersToken, { expires: 7 }); 
        	   		 	  		$.cookie("cookieUserId",headersId, { expires: 7 });
        	   		 	  		$.cookie("cookieUserName",data.username, { expires: 7 });
        	   		 	  		/*获取数据集*/
        	   		 	  		$.ajax({
									url:host+"match/dataSet",
									type:"get",
									dataType:"json",
									contentType:"application/json",
									headers:{
										"open-day-user":headersMail,
										"open-day-token":headersToken,
										"open-day-user-id":headersId,
										"open-day-match-id":headersMatchId
									},
									success:function(data){
										$("#downLink").text("数据集下载地址：");
										$("#hrefCloud").text(data.dataLink);
										$("#hrefCloud").attr("href",data.dataLink);
										$("#downPassword").text("数据集下载密码："+data.dataPassword);
									}
								});
								/*获取队伍信息*/
								$.ajax({
									url:host+"match/team",
									type:"get",
									dataType:"json",
									contentType:"application/json;charset=UTF-8",
									headers:{
										"open-day-user":headersMail,
										"open-day-token":headersToken,
										"open-day-user-id":headersId,
										"open-day-match-id":headersMatchId
									},
									success:function(data){
										$("#msgTeam").text("队伍名："+data.teamName);
										$("#msgCaptain").text("队长名："+data.captain);
										$("#msgCaptainEmail").text("队长邮箱："+data.captainMail);
										$("#msgMemberEmail").text("队员邮箱："+data.teamMemberMail);
										$("#cpJoin").hide();
									},
									error:(function(XHL,text,errorThrown) {
										if(XHL.responseJSON.message==="GET_TEAM_ID_FAILED"){
											alert("获取队伍信息失败");
										}
									})
								});
    	   		 			}  		
    	    			}
					});
				}
			}
		});
		


		/*用户注册*/
		$("#buttonSignUp").click(function() {
			var userIdTwo=$("#userIdTwo").val();
			var userPasswordTwo=$("#userPasswordTwo").val();
			var userPasswordThree=$("#userPasswordThree").val();
			var userEmailTwo=$("#userEmailTwo").val();
			if (userIdTwo==="") {
				alert("用户名不能为空");
				return false;
			}
			if (userEmailTwo==="") {
				alert("邮箱不能为空");
				return false;
			}
			if (userPasswordTwo==="") {
				alert("密码不能为空");
				return false;
			}
			if(userPasswordThree===""){
				alert("请确认密码");
				return false;
			}
			if (userPasswordTwo!=userPasswordThree) {
				alert("两次密码输入不相同");
				return false;
			}else{
				if (userIdTwo!=""&&userPasswordTwo!=""&&userPasswordThree!=""&&userEmailOne!="") {
					$.ajax({
						url:host+"/user/signup",
						type:"post",
						contentType:"application/json;charset=UTF-8",
						data:JSON.stringify({
							"username":userIdTwo,
							"password":$.md5(userPasswordTwo),
							"repassword":$.md5(userPasswordThree),
							"mail":userEmailTwo
						}),
						success:function(data){
							if (data.code===2000) {
								$.cookie("userEmailTwo", userEmailTwo, { expires: 7 }); 
        	   		 	  		$.cookie("userPasswordTwo", userPasswordTwo, { expires: 7 }); 
								alert("注册成功,请进行邮箱验证");
								$("#signIn_Up").hide();
								return false;
							}else if(data.code===2007){
								alert("邮箱发送失败");
								return false;
							}

						}
					});
				}
			}
		});


		//队伍报名


		$("#buttonTeam").click(function() {
				var captainName=$("#sign").val();
				var teamName=$("#teamName").val();
				var captainEmail=$("#userEmailOne").val();
				var memberEmail=$("#memberEmail").val();
				if(teamName===""){
					alert("队伍名不能为空");
					return false;
				}else if(memberEmail===""){
					alert("请输入队员邮箱");
					return false;
				}
				else{
					if (teamName!=""&&memberEmail!="") {
						$.ajax({
							url:host+"/match/join",
							type:"post",
							contentType:"application/json;charset=UTF-8",
							data:JSON.stringify({
								"groupName":teamName,
								"teammate":memberEmail
							}),
							headers:{
								"open-day-user":headersMail,
								"open-day-token":headersToken,
								"open-day-user-id":headersId,
								"open-day-match-id":headersMatchId
							},
							error:(function(XHL,text,errorThrown) {
								if(XHL.responseJSON.message==="NO_TEAMMATE_EXIST"){
									alert("队员邮箱没有注册");
									return false;
								}else if(XHL.responseJSON.message==="JOIN_MATCH_FAILED"){
									alert("参加比赛失败，请重试");
									return false;
								}else if(XHL.responseJSON.message==="ADD_TEAM_ERROR"){
									alert("参加比赛失败，请重试");
									return false;
								}
							}),
							success:function(data){
								if (data.code===4010) {
									$("#cpJoin").hide();
									alert("报名成功");
									$("#msgTeam").text("队伍名："+teamName);
									$("#msgCaptain").text("队长名："+$.cookie("cookieUserName"));
									$("#msgCaptainEmail").text("队长邮箱："+headersMail);
									$("#msgMemberEmail").text("队员邮箱："+memberEmail);
								}else{
									alert("报名失败");
									return false;
								}
							}
						});
					}
				}
				
			
		});
		//用户注销
		$("#signOut").click(function(){
			userEmailOne=$.cookie("cookieEmailOne");
			$.ajax({
				url:host+"/user/signout",
				type:"post",
				contentType:"application/json;charset=UTF-8",
				data:JSON.stringify({
					mail:userEmailOne,
					token:headersToken
				}),
				success:function(data){
					if (data.code===2300) {
						$("#sign").show();
						$("#userOnline").hide();
						alert("注销成功");
						$("#cpJoin").hide();
						$("#upLoadForm").hide();
						$("#downLink").text("数据集下载地址在登录后获得");
						$("#downPassword").text("数据集密码在登录后获得");
						$("#msgTeam").text("队伍名：");
						$("#msgCaptain").text("队长名：");
						$("#msgCaptainEmail").text("队长邮箱：");
						$("#msgMemberEmail").text("队员邮箱：");
						$("#joinInCp").show();
						$("#hrefCloud").text("");
						$.cookie("cookieEmailOne", null); 
						$.cookie("cookieUserId", null); 
						$.cookie("cookieToken", null); 
						signInOrUp=false;
						return signInOrUp;
					}
				}
			});
		});

		/*点击参加比赛按钮*/


		$("#joinInCp").click(function(){
			if(signInOrUp===false){
				$("#signIn_Up").show();
				formwrapper[1].style.display="none";
				formwrapper[0].style.display="block";
				signIn.style.color="#0F88EB";
				signUp.style.color=" #D5D5D5";
			}
		});



		/*获取排行榜信息*/


		$(document).ready(function(){
				$.ajax({
					url:host+"match/rankList",
					type:"get",
					dataType:"json",
					contentType:"application/json",
					success:function(data){
						$.each(data.rankList, function(i, item){
							formatDate(item.time);
							var trRank="<td>"+item.rank+"</td>";
							var trName="<td>"+item.name+"</td>";
							var	trScore="<td>"+item.score+"</td>";
							var	trTime="<td>"+date+"</td>";
							var tr=$("<tr>");
							tr.append(trRank);
							tr.append(trName);
							tr.append(trScore);
							tr.append(trTime);
							$("#rankTable").append(tr);

						})
					}
				});
		});

	/*处理时间戳*/

	function formatDate(tm) {
		var d = new Date(tm);
		date = (d.getFullYear()) + "-" + (d.getMonth() + 1) + "-" + (d.getDate());
		return date;
	}

	/*滚动效果*/

   $("#hrefTitle").click(function(){
   		$("html,body").animate({scrollTop:95},500);
   });
   $("#hrefPrize").click(function(){
   		$("html,body").animate({scrollTop:297},500);
   });
   $("#hrefTime").click(function(){
   		$("html,body").animate({scrollTop:452},500);
   });
   $("#hrefData").click(function(){
   		$("html,body").animate({scrollTop:624},500);
   });
   $("#hrefScore").click(function(){
   		$("html,body").animate({scrollTop:975},500);
   });
   $("#hrefRule").click(function(){
   		$("html,body").animate({scrollTop:1105},500);
   });
   $("#hrefTeam").click(function(){
   		$("html,body").animate({scrollTop:1487},500);
   });
   $("#hrefRank").click(function(){
   		$("html,body").animate({scrollTop:1672},500);
   });

   /*上传文件*/

 $("#upLoadButton").click(function(){
   	var formData = new FormData();
   	var upLoadFile=$('#upLoadFile');
	formData.append('answer', $('#upLoadFile')[0].files[0]);
	var a=formData.get("answer");
	if(formData===""){
		return false;
	}else{
   		$.ajax({
		    url: host+'match/upload',
		    type: "post",
		    cache: false,
		    data: formData,
		    headers:{
		    	"open-day-user":headersMail,
				"open-day-token":headersToken,
				"open-day-user-id":headersId,
				"open-day-match-id":headersMatchId
		    },
		    processData: false,
		    contentType: false,
		    success:function(data) {
		    	alert("上传文件成功");
		    	return false;
		    },
		    error:function(data) {
		    	alert("上传文件失败");
		    	return false;
		    }
		})
	}
 });

 /*图片展示*/


 $(document).ready(function(){
 	$("#activeFullPageOne").hide();
 	$("#activeFullPageTwo").hide();
 	$("#activeFullPageThree").hide();
 	$("#activeFullPageFour").hide();
 	$("#activeOne").click(function(){
 		$("#activeFullPageOne").show();
 	});
 	$("#activeTwo").click(function(){
 		$("#activeFullPageTwo").show();
 	});
 	$("#activeThree").click(function(){
 		$("#activeFullPageThree").show();
 	});
 	$("#activeFour").click(function(){
 		$("#activeFullPageFour").show();
 	});
 	$("#activeOneShow").click(function(){
 		$("#activeFullPageOne").hide();
 	});
 	$("#activeTwoShow").click(function(){
 		$("#activeFullPageTwo").hide();
 	});
 	$("#activeThreeShow").click(function(){
 		$("#activeFullPageThree").hide();
 	});
 	$("#activeFourShow").click(function(){
 		$("#activeFullPageFour").hide();
 	});
 });