$(document).ready(function(){ 
    	function getUrlParam(name){  
       	var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");  
       	var r = window.location.search.substr(1).match(reg);  
    	    if (r!=null) return unescape(r[2]);
    	    return null;  
    	}  
	$("#emailFail").hide();
	$("#emailActivation").hide();
  $("#emailTimeOut").hide();
  $("#emailInvalid").hide();
	var host="/api/";
  	var mail=getUrlParam("mail");
  	var token=getUrlParam("token");
  	if(mail!=""&&token!=""){
  		$.ajax({
  			url:host+"user/enable",
  			type:"post",
        contentType:"application/json;charset=UTF-8",
  			data:JSON.stringify({
  				mail:mail,
  				token:token
  			}),
  			success:function(data){
  				if (data.code===2103) {
  					$("#emailFail").show();
            return false;
  				}else if(data.code===2100){
  					$("#emailActivation").show();
            return false;
  				}else if(data.code===2102){
            $("#emailTimeOut").show();
            return false;
          }else if(data.code===2101){
            $("#emailInvalid").show();
            return false;
          }else{
            alert("未知异常");
            return false;
          }
  			}
  		});
  	}
});